﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tankControl : MonoBehaviour {

	public Transform tf;
	public float moveSpeed;
	public float turnSpeed;
	public Rigidbody2D rg;

	// Use this for initialization
	void Start() {
		tf = GetComponent<Transform>();
		rg = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKey(KeyCode.W)) { // Moves up
			tf.position += tf.up * moveSpeed;
		}

		if (Input.GetKey(KeyCode.S)) { // Moves down
			tf.position += -tf.up * moveSpeed;
		}

		if (Input.GetKey(KeyCode.D)) { // Rotates to the right
			tf.Rotate(0, 0, -turnSpeed);
		}

		if (Input.GetKey(KeyCode.A)) { // Rotates to the left
			tf.Rotate(0, 0, turnSpeed);
		}
	}
}