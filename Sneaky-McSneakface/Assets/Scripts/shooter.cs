﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooter : MonoBehaviour {

	public GameObject bulletPrefab;
	public int ammo;
	public Transform shootPoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (ammo > 0 && Input.GetKeyDown (KeyCode.Space)) { //Instantiates the bullet on pressing space, if they have ammo
			Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
			ammo--;
		}
		if (ammo == 0) {
			//Do nothing
		}
	}
}
