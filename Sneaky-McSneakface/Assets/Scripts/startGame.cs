﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class startGame : MonoBehaviour {

	public void LoadByIndex(int sceneIndex) { //Gives the ability to load into a different scene
		SceneManager.LoadScene (sceneIndex);
	}

}
