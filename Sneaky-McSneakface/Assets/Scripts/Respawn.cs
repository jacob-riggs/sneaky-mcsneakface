﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Respawn : MonoBehaviour {

	//Variables to be used to mess with the UI and the player
	public GameObject playerShip;
	public int Lives;
	public Button MenuButton;
	public Text ButtonText;
	public Text GameOverText;
	public Text VictoryText;
	public Text finishText;
	public Text startText;

	public float time = 3; //Time before the startText disappears

	void Start() { //Sets the UI elements to be invisible upon starting, except for startText which will destroy after a time
		MenuButton.image.color = new Color (255f, 0f, 0f, 0f);
		ButtonText.color = new Color (255f, 255f, 255f, 0f);
		GameOverText.color = new Color (255f, 0f, 0f, 0f);	
		VictoryText.color = new Color (255, 255f, 255f, 0f);
		finishText.color = new Color (255, 255f, 255f, 0f);
		Destroy (startText, time);
	}
		

	public void LoadByIndex(int sceneIndex) { //Gives the ability to switch scenes
		SceneManager.LoadScene (sceneIndex);
	}

	public void OnTriggerEnter2D(Collider2D otherCollider){

		if (otherCollider.tag == "Enemy") { //Claims that if player collides with an enemy, they must go back to the start and lose a life
			playerShip.transform.position = Vector3.zero;
			Lives--;
		}
		if (Lives == 0) { //Makes the game over text appear when the player has no lives and allows them to return to the menu
			Destroy (playerShip.gameObject);
			MenuButton.image.color = new Color (255f, 0f, 0f, 1f);
			ButtonText.color = new Color(255f, 255f, 255f, 1f);
			GameOverText.color = new Color (255f, 0f, 0f, 1f);	
		}
		if (otherCollider.tag == "Victory") { //Makes it so that the victory text appears after touching the victory item. Gives the player ability to go back to menu. Also, it destorys all remaining enemies.
			Destroy (otherCollider.gameObject);

			GameObject[] enemies;
			enemies = GameObject.FindGameObjectsWithTag ("Enemy");
			for (var i = 0; i < enemies.Length; i++) {
				Destroy (enemies [i]);
			}
			// The UI elements for victory
			MenuButton.image.color = new Color (255f, 0f, 0f, 1f);
			ButtonText.color = new Color(255f, 255f, 255f, 1f);
			VictoryText.color = new Color (255f, 0f, 0f, 1f);
			finishText.color = new Color(255f, 255f, 255f, 1f);
		}
	}
}
