﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {

	public GameObject player; // Stores the player GameObject


	private Vector3 offset; // Sets offset between camera and player

	// Use this for initialization
	void Start()
	{
		// Calculate and store the offset value
		offset = transform.position - player.transform.position;
	}

	void LateUpdate()
	{
		// Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
		transform.position = player.transform.position + offset;
	}
}