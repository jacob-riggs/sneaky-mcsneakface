﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerBullet : MonoBehaviour {

	public Transform tf;
	public float moveSpeed;
	public float lifetime;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
		DestroyObjectDelayed ();
	}
	public void Move() { //Gives the bullet movement
		tf.position += tf.up * moveSpeed;
	}

	public void DestroyObjectDelayed() { //Gives the bullet a lifetime
		Destroy (this.gameObject, lifetime);
	}

	public void OnTriggerEnter2D(Collider2D otherCollider) { //Setting up a collider for the bullet
		if (otherCollider.tag == "Enemy") { //Makes it so that it kills the enemy and destroys itself
			Destroy (otherCollider.gameObject);
			Destroy (gameObject);
		}
		if (otherCollider.tag == "Wall") { //Makes it so that if the bullet hits a wall, it destroys itself
			Destroy (gameObject);
		}
	}
}
