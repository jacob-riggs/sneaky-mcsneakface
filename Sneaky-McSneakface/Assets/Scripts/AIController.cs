﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

	public float hearingModifier; //Modifier for how good an enemy can hear
	public Transform target; //Sets the target variable

	public int canHear; //Determines if they can hear
	public int canSee; //Determines if they can see

	//Function for when AI is trying to listen
	public bool CanHear (GameObject target){
		//Is the object able to make noise?
		makeNoise makeNoise = target.GetComponent<makeNoise>();
		if (makeNoise == null) {
			return false;
		}
		//Is the object making noise?
		if(makeNoise.volume <= 0) {
			return false;
		}

		//Is the noise loud enough to be heard, aka, is it close enough
		//Find distance
		float distance = Vector3.Distance(GetComponent<Transform>().position, target.GetComponent<Transform>().position);

		//Calculate the modified distance
		float modDistance = makeNoise.volume * hearingModifier;

		//Check if close enough
		if(distance <= modDistance){
			canHear = 1;
			return true;
		}

		//Otherwise
		canHear = 0;
		return false;
	}
		
	//Function for AI to try and see player
	public float fieldOfView = 45.0f;
	public bool CanSee (GameObject target) {

		Transform targetTf = target.GetComponent<Transform> (); //Get the targets transform components
		Vector3 targetPosition = targetTf.position; //Set their transform to the target

		Vector3 agentToTargetVector = targetPosition - transform.position; //Finding the distance between the target position and the position of the AI

		float angleToTarget = Vector3.Angle (agentToTargetVector, transform.up); //Finding the angle between the two

		if (angleToTarget < fieldOfView) { //Checks to see if player is in the angle of view
			RaycastHit2D hitInfo = Physics2D.Raycast (transform.position, agentToTargetVector); //Sends a raycast from AI to check if anything is in the way
		
			if (hitInfo.collider.gameObject == target) { //If it hits the target, then it can see them
				canSee = 1;
				return true;
			}
				//Otherwise
			canSee = 0;
			return false;
		}
		//Otherwise
		canSee = 0;
		return false;
	}

	public enum AIStates { Start, Turn, Follow }; //Sets up the name for the states
	public AIStates currentState; //Sets up the variable to delcare the current state

	void FixedUpdate(){
		CanHear (GameObject.FindGameObjectWithTag("Player")); //Calls an update to always check for CanHear
		CanSee (GameObject.FindGameObjectWithTag("Player")); //Calls an update to always check for CanSee

		switch (currentState) { //Starts the switch statement to switch between states
		case AIStates.Start: //Sets up the state for Start
			//Do stuff
			DoStart();
			//Check for chance to change states
			if (canHear == 1) {
				currentState = AIStates.Turn;
			}
			if (canSee == 1) {
				currentState = AIStates.Follow;
			}
			break;
		case AIStates.Turn: //Sets up the state for Turn
			//Do stuff
			DoTurn ();
			//Check for chance to change states
			if (canSee == 1) {
				currentState = AIStates.Follow;
			}
			if (canHear == 0 && canSee == 0) {
				currentState = AIStates.Start;
			}
			break;
		case AIStates.Follow: //Sets up the state for Follow
			//Do stuff
			DoFollow ();
			//Check for a chance to change states
			if (canHear == 1 && canSee == 0) {
				currentState = AIStates.Turn;
			}
			if (canHear == 0 && canSee == 0) {
				currentState = AIStates.Start;
			}
			break;
		}
	}

	void DoStart() {
		//Do nothing
	}

	void DoTurn() { //Makes it so that the enemy will turn in the direction of the player if they are heard
		transform.rotation = Quaternion.RotateTowards (transform.rotation, target.rotation, 3f);
		Vector3 movementVector = target.position - transform.position;
		transform.up = movementVector;
	}

	void DoFollow() { //Makes it so that the enemy will follow the player if they are seen
		transform.position = Vector3.MoveTowards (transform.position, target.transform.position, .05f);
		Vector3 movementVector = target.position - transform.position;
		transform.up = movementVector;
	}
}